class Question < ActiveRecord::Base
  belongs_to :question_group
  has_many :options
  
  accepts_nested_attributes_for :options
  validates_presence_of :sequence_global
  validates_presence_of :sequence_group
  validates_presence_of :label
  #validates_presence_of :question_group_id
  
  def self.find_by_group_category(category)
    Question.joins(:question_group).where("question_groups.category = '#{category}'")
  end
end
