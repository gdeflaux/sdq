class Client < ActiveRecord::Base
  has_many :surveys
  
  GENDERS = ['boy', 'girl']
  
  validates_presence_of :name
  validates_inclusion_of :gender, :in => self::GENDERS, :message => "can't be blank"
  validates_presence_of :born_on
end
