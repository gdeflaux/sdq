class QuestionGroup < ActiveRecord::Base
  has_many :questions
  has_many :score_ranges
  accepts_nested_attributes_for :questions
  accepts_nested_attributes_for :score_ranges
  
  validates_presence_of :label
  validates_presence_of :sequence
end
