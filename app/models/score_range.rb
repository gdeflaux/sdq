class ScoreRange < ActiveRecord::Base
  belongs_to :question_group
  
  validates_presence_of :min
  validates_presence_of :max
  validates_presence_of :label
  validates_presence_of :category
  #validates_presence_of :question_group_id
  
  def self.group_score(group_id, category, score)
    g = QuestionGroup.find(group_id)
    g.score_ranges.where("category = '#{category}'").each do |sr|
      return sr.label if (score >= sr.min) and (score <= sr.max)
    end
    "oops"
  end
  
  def self.survey_score(category, score)
    ScoreRange.where("category = 'total_#{category}'").each do |sr|
      return sr.label if (score >= sr.min) and (score <= sr.max)
    end
    "total oops"
  end
  
end
