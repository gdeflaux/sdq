class Campaign < ActiveRecord::Base
  has_many :surveys, dependent: :destroy
  
  validates_presence_of :name
  validates_uniqueness_of :name
  validates_presence_of :description
  validates_presence_of :starts_on
  validates_presence_of :ends_on
  
  validate :ends_on_after_start_date
  
  def ends_on_after_start_date
    errors[:base] << "End date must be posterior to Start date" if ends_on < starts_on
  end
end
