class Option < ActiveRecord::Base
  belongs_to :question
  
  #validates_presence_of :question_id
  validates_presence_of :label
  validates_presence_of :sequence
  validates_presence_of :weight
end
