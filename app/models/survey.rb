class Survey < ActiveRecord::Base
  belongs_to :client
  belongs_to :campaign
  has_many :answers, :dependent => :destroy
  
  validates_presence_of :category, :message => "Please, tell us who you are"
  validates_presence_of :client, :message => "Select a client"
  accepts_nested_attributes_for :answers
  
  after_create :set_total_score
  before_update :update_total_score

  CATEGORIES = {
    :adult => ['parent', 'teacher'],
    :child => ['child']
  } 

  def group_category
    Survey::CATEGORIES.each_pair do |key, value|
      return key.to_s if value.include?(self.category)
    end
  end
  
  def find_answer(question_id)
    if self.answers.empty?
      return Answer.new
    else
      self.answers.each do |a|
        return a if (a.question_id == question_id)
      end
    end
  end
  
  def group_scores
    return @group_scores if defined?(@group_scores)
    @group_scores = {}

    QuestionGroup.where("question_groups.category = '#{self.group_category}'").each do |g|
      @group_scores[g.id] = 0
     end

    self.answers.includes(:question, :option).each do |a|
      @group_scores[a.question.question_group.id] += a.option.weight
    end
    @group_scores
  end
  
  private
  
  def set_total_score
    update_total_score
    save
  end
  
  def update_total_score
    s = 0
    self.answers.includes(:option).each  {|a| s += a.option.weight}
    self.score = s
  end
  
end
