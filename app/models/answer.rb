class Answer < ActiveRecord::Base
  belongs_to :survey
  belongs_to :question
  belongs_to :option
  
  validates_presence_of :question_id, :option_id
end
