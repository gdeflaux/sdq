class ClientsController < ApplicationController

  def index
    @clients = Client.all
  end
  
  def show
    @client = Client.find(params[:id])
  end
  
  def edit
    @client = Client.find(params[:id])
  end
  
  def new
    @client = Client.new
  end
  
  def update
    @client = Client.find(params[:id])
    if @client.update_attributes(params[:client])
      redirect_to(@client, notice: 'Client was successfully updated.')
    else
      render action: :edit
    end
  end
  
  def create
    @client = Client.new(params[:client])
    if @client.save
      redirect_to(client_path(@client), :notice => 'Client was successfully created.')
    else
      render action: "new"
    end
  end
  
  def search
    conditions = []
    place_holders = {}
    
    if params[:client_search_name] != ''
      conditions << "name LIKE :name"
      place_holders[:name] = '%' + params[:client_search_name] + '%'
    end
    
    if params[:client_search_dob] != ''
      conditions << "born_on = :dob"
      place_holders[:dob] = params[:client_search_dob]
    end
    
    if params[:client_search_gender] != ''
      conditions << "gender = :gender"
      place_holders[:gender] = params[:client_search_gender]
    end
    
    @clients = Client.where(conditions.join(' AND '), place_holders)

    respond_to do |format|
      format.js {render :layout => false}
    end
  end
  
end
