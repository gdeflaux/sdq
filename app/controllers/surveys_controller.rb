class SurveysController < ApplicationController
  def index
    @surveys = Survey.order("created_at DESC")
  end
  
  def show
    @survey = Survey.find(params[:id])
    @groups = QuestionGroup.includes(:questions).where("question_groups.category = '#{@survey.group_category}'").order("sequence ASC")
  end
  
  def new
    @campaign = Campaign.find(params[:campaign_id])
    @survey = Survey.new(:campaign => @campaign)
    @group_category = params[:group_category]
    @questions = Question.find_by_group_category(@group_category).includes(:options).order("sequence_global ASC")
  end
  
  def create
    @campaign = Campaign.find(params[:campaign_id])
    @survey = Survey.new(params[:survey])
    @survey.campaign = @campaign
    @group_category = params[:group_category]
    if @survey.save
      redirect_to(campaign_path(@campaign), :notice => 'Survey was successfully created.')
    else
      @questions = Question.find_by_group_category(@group_category).includes(:options).order("sequence_global ASC")
      render action: "new"
    end
  end
  
  def edit
    @survey = Survey.find(params[:id])
    @campaign = @survey.campaign
    @group_category = @survey.group_category
    @questions = Question.find_by_group_category(@group_category).includes(:options).order("sequence_global ASC")
  end
  
  def update
    @campaign = Campaign.find(params[:campaign_id])
    @survey = Survey.find(params[:id])
    @group_category = @survey.group_category
    if @survey.update_attributes(params[:survey])
      redirect_to(@survey, notice: 'Survey was successfully updated.')
    else
      @questions = Question.find_by_group_category(@group_category).includes(:options).order("sequence_global ASC")
      render action: :edit
    end
  end
  
  def destroy
    @survey = Survey.find(params[:id])
    campaign = @survey.campaign
    @survey.destroy
    redirect_to(campaign_url(campaign))
  end
  
end
