$(function() {
	$('span[id^="survey_client_selector_"]').each(function(i) {
		$(this).click(function(){
			var client_id = /survey_client_selector_([0-9]+)/.exec($(this).attr('id'))[1];
			$("#survey_client_id").val(client_id);
			$("#client-name").html($('#survey_client_selector_name_' + client_id).val());
			$("#button-modal-client").html("Change");
			$("#modal-client").dialog("close");
			$("#client_search_result").html("");
		});
	});
});