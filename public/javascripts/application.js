// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
$(function() {
  $("#campaign_starts_on").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#campaign_ends_on").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#client_search_dob").datepicker({ dateFormat: 'yy-mm-dd' });
	$("#client_born_on").datepicker({ dateFormat: 'yy-mm-dd' });
	
	$("#modal-client").dialog({
		autoOpen: false,
		height: 500,
		width: 400,
		modal: true,
		buttons: {
			Cancel: function() {$(this).dialog("close");}
		}});
		
	$("#button-modal-client")
		.click(function(){
			$("#modal-client").dialog("open");
		});
		
	$("#new-client-button-search")
		.click(function(){
			$('#client-new-fields-search').slideToggle();
		});
});