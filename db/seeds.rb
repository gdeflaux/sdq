# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

puts "Seeding database..."

groups = {}
total_ranges = []

File.open('db/fixtures/definitions.txt', 'r') do |f|
  f.each_line do |line|
    next if line =~ /^#/
    data = line.chomp.strip.split '|'
    case data[0]
      when 'g'
        groups[data[1].to_sym] = QuestionGroup.new(sequence: groups.size, label: data[3], category: data[2])
      when 'r'
        groups[data[1].to_sym].score_ranges << ScoreRange.new(category: data[2], label: data[3], min: data[4], max: data[5])
      when 'q'
        q = Question.new(label: data[2], sequence_global: data[3], sequence_group: groups[data[1].to_sym].questions.size)
        q.options << Option.new(label: data[4], weight: data[5], sequence: q.options.size)
        q.options << Option.new(label: data[6], weight: data[7], sequence: q.options.size)
        q.options << Option.new(label: data[8], weight: data[9], sequence: q.options.size)
        groups[data[1].to_sym].questions << q
      when 't'
        total_ranges << ScoreRange.new(category: data[1], label: data[2], min: data[3], max: data[4])
    end
  end
end

groups.each do |key, group|
  group.save
  puts group.errors.to_s if !group.errors.empty?
end

total_ranges.each do |range|
  range.save
  puts range.errors.to_s if !range.errors.empty?
end


puts "Done"