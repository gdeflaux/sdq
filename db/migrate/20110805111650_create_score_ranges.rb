class CreateScoreRanges < ActiveRecord::Migration
  def self.up
    create_table :score_ranges do |t|
      t.integer :min
      t.integer :max
      t.string :label, :category
      t.references :question_group

      t.timestamps
    end
  end

  def self.down
    drop_table :score_ranges
  end
end
