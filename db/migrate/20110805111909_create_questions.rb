class CreateQuestions < ActiveRecord::Migration
  def self.up
    create_table :questions do |t|
      t.integer :sequence_global
      t.integer :sequence_group
      t.string :label
      t.string :uper_label
      t.references :question_group

      t.timestamps
    end
  end

  def self.down
    drop_table :questions
  end
end
