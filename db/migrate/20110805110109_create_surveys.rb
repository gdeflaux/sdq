class CreateSurveys < ActiveRecord::Migration
  def self.up
    create_table :surveys do |t|
      t.references :client
      t.references :campaign
      t.string :category
      t.integer :score
      
      t.timestamps
    end
  end

  def self.down
    drop_table :surveys
  end
end
