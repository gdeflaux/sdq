class CreateCampaigns < ActiveRecord::Migration
  def self.up
    create_table :campaigns do |t|
      t.date :starts_on
      t.date :ends_on
      t.string :name
      t.text :description

      t.timestamps
    end
  end

  def self.down
    drop_table :campaigns
  end
end
