class CreateQuestionGroups < ActiveRecord::Migration
  def self.up
    create_table :question_groups do |t|
      t.integer :sequence
      t.string :label
      t.string :category

      t.timestamps
    end
  end

  def self.down
    drop_table :question_groups
  end
end
