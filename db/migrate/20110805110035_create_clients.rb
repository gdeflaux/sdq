class CreateClients < ActiveRecord::Migration
  def self.up
    create_table :clients do |t|
      t.string :name
      t.date :born_on
      t.string :gender

      t.timestamps
    end
  end

  def self.down
    drop_table :clients
  end
end
